#!/bin/bash

cd /var/siteBuilderAPI

if [ "$PRODUCTION" = "true" ] ; then
 echo "its prod"
else
 echo "its dev"
 git checkout develop
fi

git pull
rm package-lock.json
npm install

npm run test
